﻿using SharepointClientApp.Models;
using SharepointClientApp.SharepointLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace SharepointClientApp.Controllers
{
    public class LibraryController : ApiController
    {
        [HttpPost]
        [Route("api/library/getfolders")]
        public SharepointFolders GetFolders(PageInfo pageInfo)
        {
            var sharepointClient = new SharepointWrapper();
            var folders = sharepointClient.GetFolders(pageInfo);
            return folders;
        }

        // GET api/<controller>/5
        [HttpPost]
        [Route("api/library/getfiles")]
        public IEnumerable<SPFile> GetFiles(SharepointFolder folder)
        {
            var sharepointClient = new SharepointWrapper();
            var files = sharepointClient.GetFiles(folder);
            return files;
        }

        
        [HttpGet]
        [Route("api/library/getPageInfo")]
        public PageInfo GetPageInfo()
        {

            var sharepointClient = new SharepointWrapper();
            return sharepointClient.GetPageInfo();

        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}