﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SharepointClientApp.Models
{
    public class SPFile
    {
        public string Name { get; set; }
        public string URL { get; set; }
    }
}