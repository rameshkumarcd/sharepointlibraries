﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SharepointClientApp.Models
{
    public class PageInfo
    {
        public string NextPage { get; set; }
        public string PreviousPage { get; set; }
        public string Filter { get; set; }
        public string SortBy { get; set; }
        public bool IsAscending { get; set; }
        public bool IsNextPageInvoked { get; set; }
        public bool IsPreviousPageInvoked { get; set; }
        public int PageSize { get; set; }
        public int CurrentPage { get; set; }
    }
}