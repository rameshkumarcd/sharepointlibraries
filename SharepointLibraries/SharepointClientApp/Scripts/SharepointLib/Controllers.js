﻿app.controller("SharepointLibraryController", function ($scope, $http, $window, SharepointLibraryService) {

    
    $scope.folders = [];
    getPageInfo();
    
    $scope.folderFilter = "";
    $scope.fileFilter = "";

    $scope.sortBy = "Name";
    $scope.isAscending = true;
    $scope.isLoading = false;
    $scope.selectedNumberOfFolders = "20";

    $scope.foldersNumberOptions = ["3","5","10","20","50","100"];


    

    $scope.search = function () {

        if ($scope.folders.length == 0) {
            $scope.sortBy = "Name";
            $scope.isAscending = true;

            $("#Name").removeClass("glyphicon glyphicon-arrow-up");
            $("#Name").removeClass("glyphicon glyphicon-arrow-down");
            $("#ModifiedDateTime").removeClass("glyphicon glyphicon-arrow-up");
            $("#ModifiedDateTime").removeClass("glyphicon glyphicon-arrow-down");

            $("#Name").addClass("glyphicon glyphicon-arrow-up");


        }
        $scope.pageInfo.CurrentPage = 1;
        getSharepointLibraries();

    };

    $scope.updatePage = function () {
        $scope.pageInfo.NextPage = "";
        $scope.pageInfo.PreviousPage = "";
        $scope.pageInfo.IsNextPageInvoked = false;
        $scope.pageInfo.IsPreviousPageInvoked = false;
        $scope.pageInfo.PageSize = parseInt($scope.selectedNumberOfFolders);
        $scope.pageInfo.CurrentPage = 1;
        getSharepointLibraries();
        
    }

    $scope.navigatePage = function (isNextPage) {
        $scope.pageInfo.IsNextPageInvoked = isNextPage;
        $scope.pageInfo.IsPreviousPageInvoked = !isNextPage;

        if (isNextPage) {
            $scope.pageInfo.CurrentPage = $scope.pageInfo.CurrentPage + 1;
        } else {
            $scope.pageInfo.CurrentPage = $scope.pageInfo.CurrentPage - 1;
        }
        getSharepointLibraries();
    }

    

    $scope.sort = function (sortkey) {
        /*if ($scope.folders.length == 0) {
            return;
        }*/

        $("#Name").removeClass("glyphicon glyphicon-arrow-up");
        $("#Name").removeClass("glyphicon glyphicon-arrow-down");
        $("#ModifiedDateTime").removeClass("glyphicon glyphicon-arrow-up");
        $("#ModifiedDateTime").removeClass("glyphicon glyphicon-arrow-down");

        if (sortkey == "Name") {
            $scope.pageInfo.SortBy = "FileLeafRef";
        }
        else {
            $scope.pageInfo.SortBy = "Last_x0020_Modified";
        }

        
        
        $scope.sortBy = sortkey;
        $scope.isAscending = !$scope.isAscending;

        $scope.pageInfo.IsAscending = $scope.isAscending;
        $scope.pageInfo.NextPage = "";
        $scope.pageInfo.PreviousPage = "";
        $scope.pageInfo.IsNextPageInvoked = false;
        $scope.pageInfo.CurrentPage = 1;

        getSharepointLibraries();

        if ($scope.isAscending) {

            $("#" + sortkey).addClass("glyphicon glyphicon-arrow-up");

        }
        else {
            $("#" + sortkey).addClass("glyphicon glyphicon-arrow-down");
           
        }
        
        

        
    }

    function compareValues(key, order = 'asc') {
        return function innerSort(a, b) {
            if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
                // property doesn't exist on either object
                return 0;
            }

            const varA = (typeof a[key] === 'string')
                ? a[key].toUpperCase() : a[key];
            const varB = (typeof b[key] === 'string')
                ? b[key].toUpperCase() : b[key];

            let comparison = 0;
            if (varA > varB) {
                comparison = 1;
            } else if (varA < varB) {
                comparison = -1;
            }
            return (
                (order === 'desc') ? (comparison * -1) : comparison
            );
        };
    }

    function getSharepointLibraries() {
        $scope.isLoading = true;
        $scope.pageInfo.Filter = $scope.folderFilter;
        var promiseGet = SharepointLibraryService.getLibraries($scope.pageInfo);
        promiseGet.then(function (pl) {
            pl.data.Folders.forEach(function (e) { e.isRowHidden = true });
            $scope.isLoading = false;
            $scope.folders = pl.data.Folders;
            $scope.pageInfo = pl.data.PageInformation;

        },
            function (errorPl) {

                $scope.isLoading = false;
            });
    }

    function getPageInfo() {
        $scope.isLoading = true;
        var promiseGet = SharepointLibraryService.getPageInfo();
        promiseGet.then(function (pl) {
            
            $scope.pageInfo = pl.data;
            $scope.pageInfo.PageSize = 20;
        },
            function (errorPl) {

                $scope.isLoading = false;
            });
    }

    $scope.get = function (folder) {
        if (folder == null) {
            return;
        }
        if (folder.URL == null || folder.URL == undefined) {
            return;
        }

        $scope.isLoading = true;

        if (folder.isRowHidden == true) {
            folder.isRowHidden = false;
            folder.FileFilter = $scope.fileFilter;
            var promiseGet = SharepointLibraryService.getFiles(folder);
            promiseGet.then(function (pl) {
                $scope.isLoading = false;
                folder.files = pl.data
            },
                function (errorPl) {
                    $scope.isLoading = false;
                });
        }
        else {
            folder.isRowHidden = true;
            $scope.isLoading = false;
        }
        
    }

});