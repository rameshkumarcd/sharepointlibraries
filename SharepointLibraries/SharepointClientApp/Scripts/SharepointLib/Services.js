﻿app.service("SharepointLibraryService", function ($http) {
    //Get all folders
    this.getLibraries = function (pageInfo) {
        return $http.post("/api/Library/GetFolders/" , pageInfo);
    };

    //Get all files in the folder
    this.getFiles = function (folder) {
        return $http.post("/api/Library/GetFiles/", folder );
    };

    //Get all files in the folder
    this.getPageInfo = function () {
        
        return $http.get("/api/library/getPageInfo");
    };

});
