﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SharepointClientApp.Models
{
    public class SharepointFolder
    {
        public string Name { get; set; }
        public string URL { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime ModifiedDateTime { get; set; }
        public string FileFilter { get; set; }

    }

    public class SharepointFolders
    {
        public List<SharepointFolder> Folders { get; set; }
        public PageInfo PageInformation { get; set; }
    }

}