﻿demoApp.controller('SimpleController', function($scope, SimpleFactory) {
  //pagination variables.
  $scope.customers = [];
  $scope.filteredCustomers = [];
  $scope.currentPage = 1;
  $scope.numPerPage = 12;
  $scope.maxSize = 5;

  init();

  function init() {
    $scope.customers = SimpleFactory.getCustomers();
  }

  $scope.$watch('currentPage + numPerPage', updateFilteredItems);

  function updateFilteredItems() {
    var begin = (($scope.currentPage - 1) * $scope.numPerPage),
      end = begin + $scope.numPerPage;

    $scope.filteredCustomers = $scope.customers.slice(begin, end);
  }
});