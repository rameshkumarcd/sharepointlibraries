﻿using Microsoft.SharePoint.Client;
using SharepointClientApp.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security;
using System.Text.RegularExpressions;

namespace SharepointClientApp.SharepointLib
{
    public class SharepointWrapper
    {

        public PageInfo GetPageInfo()
        {
            return new PageInfo() { SortBy = "FileLeafRef", IsAscending=true, NextPage=string.Empty, PreviousPage= string.Empty, CurrentPage =1  };
        }

        public SharepointFolders GetFolders(PageInfo pageInfo)
        {

            List<SharepointFolder> folders = new List<SharepointFolder>();
            string url = ConfigurationManager.AppSettings["SharepointSite"];
            var ctx = new ClientContext(url);

            var password = new SecureString();
            foreach (var c in ConfigurationManager.AppSettings["Password"].ToCharArray())
                password.AppendChar(c);
            ctx.Credentials = new SharePointOnlineCredentials(ConfigurationManager.AppSettings["Username"], password);
            
            var list = ctx.Web.Lists.GetByTitle("Documents");

            var query = new CamlQuery();
            string filterXml = "<Where><Contains><FieldRef Name = 'FileLeafRef' /><Value Type = 'Lookup'>" + pageInfo.Filter + "</Value></Contains></Where>";
            string orderXml = "<OrderBy><FieldRef Name='" + pageInfo.SortBy + "' Ascending='" + pageInfo.IsAscending + "' /></OrderBy>";
            if (string.IsNullOrEmpty(pageInfo.Filter))
            {
                filterXml = string.Empty;
            }
            query.ViewXml = $"<View><Query>{filterXml}{orderXml}</Query><RowLimit>{pageInfo.PageSize}</RowLimit></View>";
            if (pageInfo.IsNextPageInvoked)
            {
                query.ListItemCollectionPosition = new ListItemCollectionPosition() { PagingInfo = pageInfo.NextPage };
            }

            if (pageInfo.IsPreviousPageInvoked)
            {
                query.ListItemCollectionPosition = new ListItemCollectionPosition() { PagingInfo = pageInfo.PreviousPage };
            }

            // Empty query to get all items
            var listItems = list.GetItems(query);

            
            ctx.Load(listItems );

            // Execute the query
            ctx.ExecuteQuery();

            
            foreach (var item in listItems)
            {
                folders.Add(new SharepointFolder
                {
                    Name = item.FieldValues["FileLeafRef"].ToString(),
                    URL = item.FieldValues["FileRef"].ToString(),
                    ModifiedDateTime = DateTime.Parse(item.FieldValues["Last_x0020_Modified"].ToString()),
                    CreatedDateTime = DateTime.Parse(item.FieldValues["Created"].ToString())
                });
            }

            bool hasPrevious = pageInfo.CurrentPage > 1;

            string columnValue = listItems[listItems.Count - 1].FieldValues[pageInfo.SortBy].ToString();
            if (listItems.ListItemCollectionPosition != null)
            {
                pageInfo.NextPage = $"Paged=TRUE&p_ID={listItems[listItems.Count - 1].Id}&p_{pageInfo.SortBy}={columnValue}";
            }
            else
            {
                pageInfo.NextPage = string.Empty;
            }

            if (hasPrevious)
            {
                columnValue = listItems[0].FieldValues[pageInfo.SortBy].ToString();
                pageInfo.PreviousPage = $"Paged=TRUE&PagedPrev=TRUE&p_ID={listItems[0].Id}&p_{pageInfo.SortBy}={columnValue}";
            }
            else
            {
                pageInfo.PreviousPage = string.Empty;
            }
            pageInfo.IsPreviousPageInvoked = false;
            pageInfo.IsNextPageInvoked = false;

            return new SharepointFolders { Folders = folders, PageInformation = pageInfo };

        }

        public List<SPFile> GetFiles(SharepointFolder folder)
        {

            // Url to site
            Uri url = new Uri(ConfigurationManager.AppSettings["SharepointSite"]);
            // get context for that Url
            var ctx = new ClientContext(url);

            // Provide credentials 
            // (Might be able to skip this if the server is on prem and your 
            // AD user has permissions to access the library)
            var password = new SecureString();
            foreach (var c in ConfigurationManager.AppSettings["Password"].ToCharArray())
                password.AppendChar(c);
            ctx.Credentials =
                new SharePointOnlineCredentials(ConfigurationManager.AppSettings["Username"], password);


            // Just to show you we have all the items now

            var sharepointFolder = ctx.Web.GetFolderByServerRelativeUrl(
            string.Format(
                @"{0}://{1}{2}",
                url.Scheme, url.Authority, folder.URL
                )
            );

            var files = ctx.LoadQuery(
                sharepointFolder.Files.Include(
                    file => file.Title,
                    file => file.ServerRelativeUrl
                )
            ).Where(f => f.ServerRelativeUrl.Substring(f.ServerRelativeUrl.LastIndexOf("/") + 1).IndexOf(folder.FileFilter, StringComparison.OrdinalIgnoreCase) >= 0);
            ctx.ExecuteQuery();

            var spFiles= (from file in files
                         select  new SPFile {  Name= file.ServerRelativeUrl.Substring(file.ServerRelativeUrl.LastIndexOf("/") + 1)
                                            , URL= file.ServerRelativeUrl }).ToList();

            /*if (string.IsNullOrEmpty(folder.FileFilter))
            {
                return spFiles;
            }

            spFiles = spFiles.Where(spFile => IsLike(folder.FileFilter, spFile.Name)).ToList();*/
            


            return spFiles;
        }

        public byte[] DownloadFile( SPFile file)
        {
            // Url to site
            Uri url = new Uri(ConfigurationManager.AppSettings["SharepointSite"]);
            // get context for that Url
            var ctx = new ClientContext(url);

            // Provide credentials 
            // (Might be able to skip this if the server is on prem and your 
            // AD user has permissions to access the library)
            var password = new SecureString();
            foreach (var c in ConfigurationManager.AppSettings["Password"].ToCharArray())
                password.AppendChar(c);
            ctx.Credentials =
                new SharePointOnlineCredentials(ConfigurationManager.AppSettings["Username"], password);

            // Open file
            var fileInformation =
                Microsoft.SharePoint.Client.File.OpenBinaryDirect(ctx, file.URL);

            // Save File
            /*using (var fileStream =
                new FileStream(@"d:\workingfolder\tes.html", FileMode.Create))
                fileInformation.Stream.CopyTo(fileStream);
                */
            var memoryStream = new MemoryStream();
            fileInformation.Stream.CopyTo(memoryStream);

            byte[] bytes = memoryStream.ToArray();
            return bytes ;
            


        }

        private bool IsLike(string pattern, string text, bool caseSensitive = false)
        {
            
            //pattern = pattern.Replace(".", @"\.");
            pattern = pattern.Replace("?", ".");
            pattern = pattern.Replace("*", ".*?");
            pattern = pattern.Replace(@"\", @"\\");
            pattern = pattern.Replace(" ", @"\s");
            pattern = pattern + "$";
            return new Regex(pattern, caseSensitive ? RegexOptions.None : RegexOptions.IgnoreCase).IsMatch(text);
        }
    }
}